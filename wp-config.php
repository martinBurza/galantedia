<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'galantedia');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '?wJUeg=:p:/W>+jPY2LWPAE? +ywqID9M4/jz1y(o>(=LRO!eLGcV4BEv@|`copk');
define('SECURE_AUTH_KEY',  ' *?k?=v.O03?upN?L6sX.gr*iDT+Dp?|Df,@f>HyV/$LocGZ-Ifyfh%]XJ<roTZY');
define('LOGGED_IN_KEY',    'QK&R?PQ!B/?f&dgF^]TftiQG6QjCwO!<NJlQ5SW:DMWUWX`+AMkr{?`~GRq2So9_');
define('NONCE_KEY',        'n2:(K((/yNdpzsx:pqLN4jUD|vrRLh[Ji9O&kWl_Se i_8/$]QtDXxLgt{`zc)W ');
define('AUTH_SALT',        'vkOhx*gncWf>8p-86mzA5I9Or!Sv@Q|69hXvYU]RT}=ck%>u=mRBOJIOdaqs#mOh');
define('SECURE_AUTH_SALT', '~AG!zRYr+Jm7/UsAI.[.IX@BXF>CwuZlpe#9v9K*T$:d#+v#51k{S>Z?l_1s+;N7');
define('LOGGED_IN_SALT',   'L<r9(;E3|h;W&-kWM81eI1UUdtaaOe+ |+;2#vN@>$akHaZI^4Y(vJ6y_{(u^`gT');
define('NONCE_SALT',       'D(A~*)N%^:M,4-Cb>Jk| @D.GGMYf-7Xg5<33l|/o$9$L,3l>}0MNf*G7wKYj&fz');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
